New Media Design & Development II
=================================
Demoproject met:

- [Laravel](http://laravel.com/docs)
- [jQuery Mobile](http://demos.jquerymobile.com/)
- [Lo-Dash](http://lodash.com/docs)
- [Modernizr](http://modernizr.com/docs/)

Installatie
-----------
Zorg ervoor dat zowel de HTTP- als de databaseserver opgestart zijn.

### 0.  Domeinnamen

Configureer deze domeinnamen in het hosts-bestand van je developmentcomputer:

    127.0.0.1   database.arteveldehogeschool.be
    127.0.0.1   nmdad-ii.arteveldehogeschool.be

Je vindt dit bestand hier:

- **Mac:** `/etc/hosts`
- **Win:** `%SystemRoot%\system32\drivers\etc\hosts`

**Tip:** voor Mac heb je [Gas Mask App](http://www.clockwise.ee/gasmask/) en voor Windows is er
bijvoorbeeld [HostsMan](http://www.abelhadigital.com/hostsman) om het beheer van het hosts-bestand te
vergemakkelijken.


### 1.  Projectmap

Open de projectmap met [PhpStorm](http://www.jetbrains.com/phpstorm/).

### 2.  Database

Maak de database aan via **MySQL Workbench** of **phpMyAdmin** door dit script uit te voeren:

    app/database/create_database_schema.sql

### 3. Configureer IDE

#### 3.1 PHP-intepreter

Voeg de PHP-interpreter toe aan het project.

- **Mac:** PhpStorm → Preferences... → PHP

        /Applications/mampstack-5.4.26-2/php/bin

- **Win:** File → Settings... → PHP

        C:\BitNami\wampstack-5.4.26-2\php

#### 3.2 Composer

Integreer [Composer](https://getcomposer.org/) in het PhpStorm-project:

- **Mac:** PhpStorm → Preferences... → Command Line Tool Support → + (Add) → Composer
- **Win:** File → Settings... → Command Line Tool Support → + (Add) → Composer

Gebruik `Visibility: global` zodat je dit voor nieuwe projecten niet meer moet instellen.

Selecteer het tool en klik op Edit om de Alias te hernoemen naar **'composer'**

Tools → Run Command...

    composer update

#### 3.3 Laravel Artisan

Integreer [Artisan](http://laravel.com/docs/artisan) in het PhpStorm-project:

- **Mac:** PhpStorm → Preferences... → Command Line Tool Support → + (Add) → Tool based on Symfony Console
- **Win:** File → Settings... → Command Line Tool Support → + (Add) → Tool based on Symfony Console

Tools → Run Command...

    artisan migrate --seed

of indien je al een migration uitgevoerd hebt:

    artisan migrate:refresh --seed
