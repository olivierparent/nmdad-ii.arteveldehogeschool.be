<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2014 Artevelde University College Ghent
 */

class UserController extends \BaseController {

    protected $layout = 'layouts.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function index()
    {
        $this->layout->content = View::make('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->layout->content = View::make('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // Zie: http://laravel.com/docs/validation
        $rules = [
            'email'            => 'required|email|max:255',
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'first_name'       => 'required|min:2|max:40',
            'last_name'        => 'required|min:2|max:40',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $user = new User(Input::all());
            $user->password = Input::get('password'); // Hash wordt in het model geregeld via het 'creating' event!
            $user->save();

            return Redirect::route('user.login'); // Zie: $ php artisan routes
            //
        } else {

            return Redirect::route('user.create') // Zie: $ php artisan routes
                ->withInput()            // Vul het formulier opnieuw in met de Input.
                ->withErrors($validator) // Maakt $errors in View.
            ;
        }
    }

    /**
     * Toon het aanmeldformulier.
     */
    public function login()
    {
        $this->layout->content = View::make('user.login');
    }

    /**
     * Authenticeer de gebruiker.
     *
     * @return Response
     */
    public function auth()
    {
        // Zie: http://laravel.com/docs/validation
        $rules = [
            'email'    => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $credentials = [
                'email'      => Input::get('email'),
                'password'   => Input::get('password'),
                'deleted_at' => null, // Extra voorwaarde.
            ];
            $remember = Input::get('switch-auth') == 'remember'; // Onthoud de authenticatie.
//            Log::info('Remember: ' . $remember);

            // Zie: http://laravel.com/docs/security#authenticating-users
            if (Auth::attempt($credentials, $remember)) {

                return Redirect::intended('/'); // Stuur de aangemelde bezoeker terug naar de URI vanwaar de bezoeker komt. Indien de bezoeker geen toegang (meer) heeft tot die URI, wordt naar de root ('/') van de website doorgestuurd.
            } else {

                return Redirect::route('user.login')
                    ->withInput()             // Vul het formulier opnieuw in met de Input.
                    ->with('auth-error-message', 'Voer een geldig e-mailadres en wachtwoord in.')
                ;
            }
        } else {

            return Redirect::route('user.login') // Zie: $ php artisan routes
                ->withInput()            // Vul het formulier opnieuw in met de Input.
                ->withErrors($validator) // Maakt $errors in View.
            ;
        }
    }

}
