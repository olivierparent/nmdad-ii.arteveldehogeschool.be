<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->command->info('Seeding table `user`');
        $this->call('UserTableSeeder');

        $this->command->info('Seeding table `label`');
        $this->call('LabelTableSeeder');

        $this->command->info('Seeding table `task`');
        $this->call('TaskTableSeeder');

        $this->command->info('Seeding table `pomodoro`');
        $this->call('PomodoroTableSeeder');

        $this->command->info('Seeding table `label_pomodoro`');
        $this->call('LabelPomodoroTableSeeder');
	}

}
