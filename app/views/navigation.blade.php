        <div data-role="header" data-position="fixed">
            <div data-role="navbar">
                <ul>
                    <li>{{ HTML::linkRoute('user.index', 'Start', [], [
                        'class' => 'ui-btn ui-btn-inline ui-btn-icon-left ui-icon-home',
                        ]) }}</li>
                    <li><a href="#page-tasks" class="{{{ $pageActive == 'page-tasks' ? 'ui-btn-active ui-state-persist ' : '' }}}ui-btn-icon-left ui-icon-bullets">Taken</a></li>
                    <li><a href="#page-labels" class="{{{ $pageActive == 'page-labels' ? 'ui-btn-active ui-state-persist ' : '' }}}ui-btn-icon-left ui-icon-tag">Labels</a></li>
                </ul>
            </div><!-- /navbar -->
        </div><!-- /header -->
