<?php
$version = [
    'jQuery-legacy' => '1.11.1', // Previous generation of jQuery
    'jQuery'        => '2.1.1',
    'jQueryMobile'  => '1.4.3',
    'lodash'        => '2.4.1',
    'modernizr'     => '2.8.2',
];
?><!doctype html>
<html lang="{{ Config::get('app.locale') }}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title>{{ HTML::entities('New Media Design & Development II') }}</title>
    <link rel="dns-prefetch" href="{{ '//', $_SERVER['HTTP_HOST'], '/' }}">
    <link rel="dns-prefetch" href="//code.jquery.com/">
    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com/">
    {{ HTML::style("//code.jquery.com/mobile/{$version['jQueryMobile']}/jquery.mobile-{$version['jQueryMobile']}.min.css") }}
    {{ HTML::style("styles/app.css") }}
    {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/lodash.js/{$version['lodash']}/lodash.mobile.min.js") }}
    {{ HTML::script("//code.jquery.com/jquery-{$version['jQuery']}.min.js") }}
@yield('head')
    {{ HTML::script("//code.jquery.com/mobile/{$version['jQueryMobile']}/jquery.mobile-{$version['jQueryMobile']}.min.js") }}
    {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/modernizr/{$version['modernizr']}/modernizr.min.js") }}
</head>
<body>
@yield('content')
</body>
</html>
