@extends('layouts.master')
@section('head')
    {{ HTML::script('scripts/utilities.js') }}
    {{ HTML::script('scripts/models/Pomodoro.js') }}
    {{ HTML::script('scripts/models/Label.js') }}
    {{ HTML::script('scripts/models/Task.js') }}
    {{ HTML::script('scripts/app.js') }}
@stop

@section('content')
    <div id="page-tasks" data-role="page">
        @include('navigation', ['pageActive' => 'page-tasks'])
        <div role="main" class="ui-content">
            <div class="ui-grid-b">
                <div class="ui-block-a"></div>
                <div class="ui-block-b">
                    <h1>Taken</h1>
                    <div id="tasks" data-role="collapsibleset" data-iconpos="right"></div>
                    <form id="form-task-add" action="">
                        <input type="text" name="task-name" id="task-name" placeholder="Nieuwe Taak toevoegen&hellip;" value="">
                    </form>
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->

    <div id="page-pomodoro" data-role="page">
        <div role="main" class="ui-content">
            <div class="ui-grid-b">
                <div class="ui-block-a"></div>
                <div class="ui-block-b">
                    <h1>Pomodoro</h1>
                    <div class="ui-corner-all custom-corners">
                        <div class="ui-bar ui-bar-a">
                            <h3></h3>
                        </div>
                        <div class="ui-body ui-body-a">
                            <form id="form-pomodoro-update" action="">
                                <textarea name="pomodoro-description" id="pomodoro-description"></textarea>
                                <select multiple="multiple" data-native-menu="false" name="pomodoro-labels" id="pomodoro-labels" data-mini="true">
                                    <option>Labels</option>
                                    <optgroup id="pomodoro-labels-personal" label="Persoonlijk"></optgroup>
                                    <optgroup id="pomodoro-labels-shared" label="Gedeeld"></optgroup>
                                </select>
                                <div class="ui-grid-b">
                                    <div class="ui-block-a">
                                        <button id="pomodoro-timer" class="ui-btn ui-corner-all ui-btn-b ui-mini ui-btn-icon-left ui-icon-forbidden" data-pomodoro-time=""></button>
                                    </div>
                                    <div class="ui-block-b"></div>
                                    <div class="ui-grid-c">
                                        <button id="pomodoro-destroy" class="ui-btn ui-corner-all ui-mini ui-btn-icon-left ui-icon-delete">Verwijder</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <a class="ui-btn ui-btn-b ui-btn-inline ui-icon-back ui-btn-icon-left" href="#page-tasks" data-rel="back" data-direction="reverse" data-transition="turn">Taken</a>
                </div>
            </div>
            {{-- Download van: http://translate.google.com/translate_tts?ie=UTF-8&tl=en&q=Pomodoro+finished& --}}
            <audio id="sound-pomodoro-paused">
                <source src="\assets\sounds\pomodoro_paused.mp3" type="audio/mpeg">
            </audio>
            <audio id="sound-pomodoro-started">
                <source src="\assets\sounds\pomodoro_started.mp3" type="audio/mpeg">
            </audio>
            <audio id="sound-pomodoro-finished">
                <source src="\assets\sounds\pomodoro_finished.mp3" type="audio/mpeg">
            </audio>
        </div><!-- /content -->
    </div><!-- /page -->

    <div id="page-labels" data-role="page">
        @include('navigation', ['pageActive' => 'page-labels'])
        <div role="main" class="ui-content">
            <div class="ui-grid-b">
                <div class="ui-block-a"></div>
                <div class="ui-block-b">
                    <h1>Labels</h1>
                    <ul id="labels" data-role="listview" data-filter="true" data-filter-placeholder="Labels zoeken&hellip;" data-inset="true" data-split-icon="delete"></ul>
                    <form id="form-label-add" action="">
                        <input type="text" name="label-name" placeholder="Nieuw Label toevoegen&hellip;" value="">
                    </form>
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->

    <div id="page-label" data-role="page" data-dialog="true" data-close-btn="right">
        <div data-role="header">
            <h1>Label bewerken</h1>
        </div>
        <div role="main" class="ui-content">
            <form id="form-label-update" action="">
                <div class="ui-field-contain">
                    <label for="label-name">Naam:</label>
                    <input type="text" name="label-name" id="label-name" placeholder="Naam" value="">
                </div>
                <div class="ui-field-contain">
                    <label for="label-colour">Kleur:</label>
{{--                <input type="text" name="label-colour" id="label-colour" pattern="[#][0-9A-Fa-f]{6}" placeholder="#RRGGBB" maxlength="7" value=""> --}}
                    <input type="color" name="label-colour" id="label-colour" pattern="[#][0-9A-Fa-f]{6}" placeholder="#RRGGBB" maxlength="7" value="">
                </div>
                <div class="ui-field-contain">
                    <label></label>
                    <button class="ui-btn ui-corner-all ui-btn-b ui-btn-inline ui-mini ui-btn-icon-left ui-icon-check">Opslaan</button>
                </div>
            </form>
        </div><!-- /content -->
    </div><!-- /page -->
@stop
