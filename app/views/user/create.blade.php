@section('content')
<div class="ui-grid-b">
    <div class="ui-block-a"></div>
    <div class="ui-block-b">
        <h1>Registreer je</h1>
@if ($errors->any())
        <div class="ui-corner-all">
            <div class="ui-bar ui-bar-a">
                <h3>Kan niet registeren</h3>
            </div>
            <div class="ui-body ui-body-a">
                <p>Controleer de in <span class="error">rood</span> aangeduide velden.</p>
                <ul>
@foreach ($errors->all('<li>:message</li>' . PHP_EOL) as $message)
                {{ $message }}
@endforeach
                </ul>
            </div>
        </div>
@endif
        {{ Form::open([
            'route' => 'user.store',
            'data-ajax' => 'false',
        ]), PHP_EOL }}
        <fieldset>
            <legend class="ui-hidden-accessible">Aanmeldgegevens</legend>

<?php $label = ucfirst(Lang::get('validation.attributes.email')); ?>
            {{ Form::label('email', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('email') ? ' error' : '' }}}">
                {{ Form::email('email', '', [
                    'placeholder' => $label,
                    'data-enhanced' => 'true',
                ]), PHP_EOL }}
@if ($errors->has('email'))
                {{ $errors->first('email', '<small class="ui-bar">:message</small>') }}
@endif
            </div>

<?php $label = ucfirst(Lang::get('validation.attributes.password')); ?>
            {{ Form::label('password', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('password') ? ' error' : '' }}}">
                {{ Form::password('password', [
                    'placeholder' => $label,
                    'data-enhanced' => 'true',
                ]), PHP_EOL }}
@if ($errors->has('password'))
                {{ $errors->first('password', '<small class="ui-bar">:message</small>') }}
@endif
            </div>

<?php $label = ucfirst(Lang::get('validation.attributes.confirm_password')); ?>
            {{ Form::label('confirm_password', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('confirm_password') ? ' error' : '' }}}">
                {{ Form::password('confirm_password', [
                    'placeholder' => $label,
                    'data-enhanced' => 'true',
                ]), PHP_EOL }}
@if ($errors->has('confirm_password'))
                {{ $errors->first('confirm_password', '<small class="ui-bar">:message</small>') }}
@endif
            </div>
        </fieldset>

        <fieldset>
            <legend class="ui-hidden-accessible">Personalia</legend>
<?php $label = ucfirst(Lang::get('validation.attributes.first_name')); ?>
            {{ Form::label('first_name', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('first_name') ? ' error' : '' }}}">
                {{ Form::text('first_name', '', [
                    'placeholder' => $label,
                    'data-enhanced' => 'true',
                ]), PHP_EOL }}
@if ($errors->has('first_name'))
                {{ $errors->first('first_name', '<small class="ui-bar">:message</small>') }}
@endif
            </div>

<?php $label = ucfirst(Lang::get('validation.attributes.last_name')); ?>
            {{ Form::label('last_name', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('last_name') ? ' error' : '' }}}">
                {{ Form::text('last_name', '', [
                    'placeholder' => $label,
                    'data-enhanced' => 'true',
                ]), PHP_EOL }}
@if ($errors->has('last_name'))
                {{ $errors->first('last_name', '<small class="ui-bar">:message</small>') }}
@endif
            </div>
        </fieldset>

        <div class="ui-input-btn ui-btn ui-btn-inline ui-btn-b">
            Registreren
            {{ Form::submit('Registreren', [
                'data-enhanced' => 'true'
            ]), PHP_EOL }}
        </div>
        {{ HTML::linkRoute('user.index', 'Terug naar de startpagina', [], [
            'class' => 'ui-btn ui-btn-inline ui-btn-icon-left ui-icon-home',
            'data-ajax' => 'false',
        ]), PHP_EOL }}

        {{ Form::close(), PHP_EOL }}
    </div>
</div>
@stop