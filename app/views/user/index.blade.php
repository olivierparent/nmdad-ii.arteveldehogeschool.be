@section('content')
    <div class="ui-grid-b">
        <div class="ui-block-a"></div>
        <div class="ui-block-b">
            <h1>Startpagina</h1>
@if ( Auth::check() )

            <p>Welkom terug <strong>{{ Auth::user()->first_name , ' ', Auth::user()->last_name }}</strong>!</p>
@if ( Auth::viaRemember() )
            <p>Je bent automatisch aangemeld omdat je je met optie 'Onhouden' aangemeld hebt.</p>
@endif
@else
            <p>Welkom bezoeker!</p>
@endif

@if ( Auth::guest() )
            {{-- Link maken van een Named Route http://laravel.com/api/class-Illuminate.Html.HtmlBuilder.html#_linkRoute --}}
            <p>{{ HTML::linkRoute('user.login', 'Meld je aan', [], [
                    'class'     => 'ui-btn ui-btn-b ui-btn-inline',
                    'data-ajax' => 'false',
                ]) }}
                {{ HTML::linkRoute('user.create', 'Registreer je', [], [
                    'class'     => 'ui-btn ui-btn-inline',
                    'data-ajax' => 'false',
            ]) }}</p>
@else
            <p>{{ HTML::link('/', 'Ga naar de taken', [
                    'class'     => 'ui-btn ui-btn-inline ui-btn-b ui-btn-icon-left ui-icon-bullets',
                    'data-ajax' => 'false',
                ]) }}
                {{ HTML::linkRoute('user.logout', 'Meld je af', [], [
                    'class'     => 'ui-btn ui-btn-inline ui-btn-icon-left ui-icon-power',
                    'data-ajax' => 'false',
                ]) }}
@endif
        </div>
    </div>
@stop
