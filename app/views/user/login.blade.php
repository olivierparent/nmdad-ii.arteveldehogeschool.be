@section('content')
    <div class="ui-grid-b">
        <div class="ui-block-a"></div>
        <div class="ui-block-b">
            <h1>Meld je aan</h1>
@if ($errors->any())
            <div class="ui-corner-all">
                <div class="ui-bar ui-bar-a">
                    <h3>Kan niet aanmelden</h3>
                </div>
                <div class="ui-body ui-body-a">
                    <p>Controleer de in <span class="error">rood</span> aangeduide velden.</p>
                    <ul>
@foreach ($errors->all('<li>:message</li>' . PHP_EOL) as $message)
                        {{ $message }}
@endforeach
                    </ul>
                </div>
            </div>
@elseif(Session::has('auth-error-message'))
            <div class="ui-corner-all">
                <div class="ui-bar ui-bar-a">
                    <h3>Kan niet aanmelden</h3>
                </div>
                <div class="ui-body ui-body-a">
                    <p>{{ Session::get('auth-error-message') }}</p>
                </div>
            </div>
@endif
            {{ Form::open([
                'route' => 'user.auth',
                'data-ajax' => 'false',
            ]), PHP_EOL }}

            <fieldset>
                <legend class="ui-hidden-accessible">Aanmeldgegevens</legend>

<?php $label = ucfirst(Lang::get('validation.attributes.email')); ?>
                {{ Form::label('email', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
                <div class="ui-input-text ui-body-inherit{{{ $errors->has('email') ? ' error' : '' }}}">
                    {{ Form::email('email', '', [
                        'placeholder' => $label,
                        'data-enhanced' => 'true',
                    ]), PHP_EOL }}
@if ($errors->has('email'))
                    {{ $errors->first('email', '<small class="ui-bar">:message</small>') }}
@endif
                </div>

<?php $label = ucfirst(Lang::get('validation.attributes.password')); ?>
                {{ Form::label('password', $label . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
                <div class="ui-input-text ui-body-inherit{{{ $errors->has('password') ? ' error' : '' }}}">
                    {{ Form::password('password', [
                        'placeholder' => $label,
                        'data-enhanced' => 'true',
                    ]), PHP_EOL }}
@if ($errors->has('password'))
                    {{ $errors->first('password', '<small class="ui-bar">:message</small>') }}
@endif
                </div>
                <div class="ui-field-contain">
                    <label for="switch-auth" class="">Onthouden:</label>
                    <select name="switch-auth" id="switch-auth" data-role="slider">
                        <option value="forget">Nee</option>
                        <option value="remember" selected>Ja</option>
                    </select>
                </div>
            </fieldset>

            <div class="ui-input-btn ui-btn ui-btn-inline ui-btn-b">
                Aanmelden
                {{ Form::submit('Aanmelden', ['data-enhanced' => 'true']), PHP_EOL }}
            </div>
            {{ HTML::linkRoute('user.index', 'Terug naar de startpagina', [], [
                'class' => 'ui-btn ui-btn-inline ui-btn-icon-left ui-icon-home',
                'data-ajax' => 'false',
            ]), PHP_EOL }}

            {{ Form::close(), PHP_EOL }}
        </div>
    </div>
@stop