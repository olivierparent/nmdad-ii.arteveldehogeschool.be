<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Bekijk alle alle routes met `artisan routes`
 */

Route::get('/', function() {
    return View::make('app');
    //return View::make('hello');
})->before('auth'); // Pas de 'auth' filter toe (zie `filters.php`).


Route::resource('user', 'UserController', [
    'only' => ['index', 'create', 'store'], // Enkel deze Controller Actions worden gebruikt voor deze Route.
]);

Route::group([
        'prefix' => 'user', // Begin de Routes van deze groep met 'user/'
    ], function () {
        Route::group([
                'before' => 'guest', // Pas de 'guest' filter toe op de Routes van deze groep (zie `filters.php`).
            ], function () {
                Route::get('login', [
                    'as'   => 'user.login', // Naam voor deze Route.
                    'uses' => 'UserController@login', // Deze Route is gekoppeld aan de 'login' Controller Action van de 'User' Controller.
                ]);
                Route::post('auth', [
                    'as'   => 'user.auth', // Naam voor deze Route.
                    'uses' => 'UserController@auth' // Deze Route is gekoppeld aan de 'auth' Controller Action van de 'User' Controller.
                ]);
            }
        );

        Route::get('logout', [
            'as'   => 'user.logout',
            function () {
                Auth::logout();

                return Redirect::route('user.index');
            }
        ])->before('auth'); // Pas de 'auth' filter toe (zie filters.php).
    }
);

/**
 * De api groupeert enkele resource controllers die enkel door geauthenticeerde bezoekers gebruikt mogen worden.
 */
Route::group([
        'before' => 'auth', // Pas de 'auth' filter toe op de Routes van deze groep (zie `filters.php`).
        'prefix' => 'api',  // Begin deze routes met 'api/'
    ], function () {
        Route::resource('label', 'LabelController', [
            'except' => ['create', 'edit'], // De Controller Actions worden niet gebruikt voor deze Route.
        ]);
        Route::resource('pomodoro', 'PomodoroController', [
            'except' => ['create', 'edit'], // De Controller Actions worden niet gebruikt voor deze Route.
        ]);
        Route::resource('task', 'TaskController', [
            'except' => ['create', 'edit'], // Deze Controller Actions worden niet gebruikt voor deze Route.
        ]);
    }
);
