function Pomodoro()
{
    // Properties
    this.description = null;
    this.time_remaining = "00:00";
    this.task_id = null;
    this.labels = [];

    // Methods
    this.getDescription = function()
    {
        return this.description;
    };
    this.setDescription = function(description)
    {
        this.description = description;
    };

    this.getTimeRemaining = function()
    {
        return this.time_remaining;
    };
    this.setTimeRemaining = function(time_remaining)
    {
        this.time_remaining = time_remaining;
    };

    this.getTaskId = function()
    {
        return this.task_id;
    };
    this.setTaskId = function(task_id)
    {
        this.task_id = task_id;
    };

    this.addLabel = function(label_id)
    {
        var label = new Label();
        label.setId(label_id);

        this.labels.push(label);
    }
    this.addLabels = function(label_ids)
    {
        var that = this;

        return _(label_ids).each(function(label_id) {
            that.addLabel(label_id);
        });
    }
}
