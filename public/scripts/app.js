$(document).on("mobileinit", function() {
    console.info("jQuery", $.prototype.jquery
        , "|", "jQuery Mobile", $.mobile.version
        , "|", "Lo-Dash", _.VERSION
//        , "|", "Modernizr", Modernizr._version
    );
    /**
     * jQuery Mobile defaults: http://api.jquerymobile.com/global-config/
     */
    $.mobile.ajaxEnabled = false;
    $.mobile.allowCrossDomainPages = true;/* $.support.cors = true; // needed for Apache Cordova or Adobe PhoneGap */
    $.mobile.loader.prototype.options.html = "";
    $.mobile.loader.prototype.options.textVisible = false;
    $.mobile.loader.prototype.options.theme = "b";
    $.mobile.ns = ""; // Set a namespace for jQuery Mobile data attributes
    $.mobile.page.prototype.options.theme = "a";
    $.mobile.popup.prototype.options.overlayTheme = "b";
    $.ajaxSetup({
        cache: false,
        contentType: "application/json",
        dataType: "json",
        timeout: 30 * 1000 // 30 seconds to timeout
    });
    App.init();
});

/**
 * App object with properties and methods
 */
var App = {
    baseUrl: "/api/",
    init: function() {
        this.bindEvents();
        this.load("task").done(this.renderTasks);
        this.load("label")
            .done(function(labels) {
                Util.Store.session("labels", labels);
            })
        ;
        console.info("App initialized");
    },
    bindEvents: function() {
        var that = this;

        $(document)
            .on("blur", "#form-pomodoro-update textarea", function(ev) {
                var $form = $(this).parents("form").first();
                that.updatePomodoro($form);
            })
            .on("change", "#form-pomodoro-update select", function(ev) {
                var $form = $(this).parents("form").first();
                that.updatePomodoro($form);
            })
            // Destroy Label
            .on("click", "[data-label-id] a[data-label-destroy]", function(ev) {
                var id = $(this).parent().attr("data-label-id");
                that.destroyLabel(id);
            })
            // Destroy Pomodoro
            .on("click", "#pomodoro-destroy", function(ev) {
                ev.preventDefault();
                var id = Util.Store.session("pomodoro-id");
                that.destroyPomodoro(id)
                    .done(function() {
                        that.load("task").done(that.renderTasks);
                        $.mobile.navigate("#page-tasks");
                    })
                ;
            })
            // Destroy Task
            .on("click", "[data-role=collapsible] button", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-task-id");
                that.destroyTask(id);
            })
            // Destroy Task show/hide button
            .on("listviewcreate", "#page-tasks [data-role=listview]", function(ev, ui) {
                var $button = $(this).prev("button");
                if (0 < $(this).children("li").length) {
                    $button.hide();
                } else {
                    $button.show();
                }
            })
            // Load model per page
            .on("pagecontainerbeforeshow", "body", function(ev, ui) {
                var activePage = $(this).pagecontainer("getActivePage").attr("id");
                switch (activePage) {
                    case "page-label":
                        var id = Util.Store.session("label-id");
                        if (!_.isNull(id)) {
                            that.load("label", id).done(that.renderLabel);
                        }
                        break;
                    case "page-labels":
                        that.load("label").done(that.renderLabels);
                        break;
                    case "page-pomodoro":
                        var id = Util.Store.session("pomodoro-id");
                        if (!_.isNull(id)) {
                            that.load("pomodoro", id).done(that.renderPomodoro);
                        }
                        break;
                    default:
                        console.warn("Unknown page '" + activePage + "'.");
                        break;
                }
            })
            // Set active Label
            .on("click", "[data-label-id] a[href=#page-label]", function(ev) {
                var id = Number($(this).parent().attr("data-label-id"));
                Util.Store.session("label-id", id);
            })
            // Set active Pomodoro
            .on("click", "[data-pomodoro-id] a", function(ev) {
                var id = Number($(this).parents("[data-pomodoro-id]").attr("data-pomodoro-id"));
                Util.Store.session("pomodoro-id", id);
            })
            // Store Label
            .on("submit", "#form-label-add", function(ev) {
                ev.preventDefault();
                var $form = $(this);
                that.storeLabel($form);
            })
            // Store Pomodoro
            .on("submit", "[data-role=collapsible] form", function(ev) {
                ev.preventDefault();
                var $form = $(this);
                that.storePomodoro($form);
            })
            // Store Task
            .on("submit", "#form-task-add", function(ev) {
                ev.preventDefault();
                var $form = $(this);
                that.storeTask($form);
            })
            // Timer
            .on("click", "#pomodoro-timer", function(ev) {
                ev.preventDefault();
                if (_.isNull(that.Timer.data.intervalId)) {
                    that.Timer.start();
                    $(this)
                        .removeClass("ui-icon-forbidden")
                        .addClass("ui-icon-clock")
                    ;
                    $("#sound-pomodoro-started")[0].play();
                } else {
                    that.Timer.stop();
                    var $form = $(this).parents("#form-pomodoro-update").first();
                    that.updatePomodoro($form);
                    $(this)
                        .removeClass("ui-icon-clock")
                        .addClass("ui-icon-forbidden")
                    ;
                    $("#sound-pomodoro-paused")[0].play();
                }
            })
            // Update Label
            .on("submit", "#form-label-update", function(ev) {
                ev.preventDefault();
                var $form = $(this);
                that.updateLabel($form);
            })
        ;
    },
    destroy: function(modelName, id) {
        var that = this;

        return $.ajax({
            type: "DELETE",
            url: that.baseUrl + modelName + "/" + id,
            beforeSend: function(jqXHR) {
                $.mobile.loading("show");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                that.logErrors(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
                $.mobile.loading("hide");
            }
        });
    },
    destroyLabel: function(id) {
        var $label = $("[data-label-id=" + id + "]");
        $label.hide();
        this.destroy("label", id)
            .fail(function() {
                $label.show();
            })
        ;
    },
    destroyPomodoro: function(id) {
        return this.destroy("pomodoro", id);
    },
    destroyTask: function(id) {
        var $task = $("[data-task-id=" + id + "]");
        $task.hide();
        this.destroy("task", id)
            .fail(function() {
                $task.show();
            })
        ;
    },
    load: function(modelName, id) {
        var that = this;

        return $.ajax({
            type: "GET",
            url: that.baseUrl + modelName + (_.isUndefined(id) ? "" : "/" + id),
            beforeSend: function(jqXHR) {
                $.mobile.loading("show");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                that.logErrors(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
                $.mobile.loading("hide");
            }
        });
    },
    renderLabel: function(label) {
        var that = this;

        $("#page-label")
            .find("#label-name")
                .val(label.name)
            .end()
            .find("#label-colour")
                .val("#" + label.colour)
        ;
    },
    renderLabels: function(labels) {
        var compiledLabelTemplates = "";
        $.each(labels, function(i, label) {
            compiledLabelTemplates += _.template(App.templates.label, { "label": label });
        });
        $("#labels")
            .html(compiledLabelTemplates)
            .trigger("listviewcreate")
            .listview().listview("refresh")
        ;
    },
    renderPomodoro: function(pomodoro) {
        var that = this;

        var compiledLabelTemplates = {
                personal: "",
                shared: ""
            },
            labels      = Util.Store.session("labels"),
            selectedIds = _.pluck(pomodoro.labels, 'id');
        $.each(labels, function(i, label) {
            var compiledLabelTemplate = _.template(App.templates.pomodoroLabel, {
                "label": label,
                "selected": _.contains(selectedIds, label.id)
            });
            if (_.isNull(label.user_id)) {
                compiledLabelTemplates.shared += compiledLabelTemplate;
            } else {
                compiledLabelTemplates.personal += compiledLabelTemplate;
            }
        });
        $("#page-pomodoro")
            .find("h3")
              .html(pomodoro.task.name)
            .end()
            .find("#pomodoro-description")
                .html(pomodoro.description)
            .end()
            .find("#pomodoro-description")
                .html(pomodoro.description)
            .end()
            .find("#pomodoro-labels")
                .find("[value]")
                    .remove()
                .end()
                .find("#pomodoro-labels-personal")
                    .html(compiledLabelTemplates.personal)
                .end()
                .find("#pomodoro-labels-shared")
                    .html(compiledLabelTemplates.shared)
                .end()
                .selectmenu("refresh", true) /* met true wordt de refresh altijd uitgevoerd */
        ;
        App.Timer.set(pomodoro.time_remaining);
    },
    renderTask: function(task) {
        var compiledTaskTemplate = _.template(this.templates.task, { "task": task });
        $("#tasks")
            .append(compiledTaskTemplate)
            .trigger("create")
        ;
    },
    renderTaskPomodoro: function(pomodoro, $task) {
        var compiledPomodoroTemplate = _.template(this.templates.pomodoro, { "pomodoro": pomodoro });
        $task.find("ul")
            .first()
                .append(compiledPomodoroTemplate)
                .trigger("listviewcreate")
                .listview().listview("refresh")
        ;
    },
    renderTasks: function(tasks) {
        var that = this;

        var compiledTaskTemplates = "";
        $.each(tasks, function(i, task) {
            var compiledPomodoroTemplates = "";
            $.each(task.pomodori, function(j, pomodoro) {
                compiledPomodoroTemplates += _.template(App.templates.pomodoro, { "pomodoro": pomodoro });
            });
            compiledTaskTemplates +=
                $(_.template(App.templates.task, { "task": task }))
                    .find("ul")
                        .append(compiledPomodoroTemplates)
                    .end()
                    .prop("outerHTML")
            ;
        });
        $("#tasks")
            .html(compiledTaskTemplates)
//              .collapsibleset("refresh")
            .trigger("create")
        ;
    },
    store: function(modelName, model) {
        var that = this;

        return $.ajax({
            type: "POST",
            url: that.baseUrl + modelName,
            data: JSON.stringify(model),
            error: function(jqXHR, textStatus, errorThrown) {
                that.logErrors(jqXHR, textStatus, errorThrown);
            }
        });
    },
    storeLabel: function($form) {
        var that = this;

        var $nameInput = $form.find("input[name=label-name]"),
            label      = new Label();
        label.setName($nameInput.val().trim());

        that.store("label", label)
            .done(function() {
                $nameInput.val("");
                that.load("label").done(that.renderLabels);
            })
        ;
    },
    storePomodoro: function($form) {
        var that = this;

        var $nameInput = $form.find("input[name=pomodoro-name]"),
            $task      = $nameInput.parents("[data-role=collapsible]").first(),
            pomodoro   = new Pomodoro();
        pomodoro.setDescription($nameInput.val().trim());
        pomodoro.setTaskId($task.attr("data-task-id"));

        that.store("pomodoro", pomodoro)
            .done(function(pomodoro) {
                $nameInput.val("");
                that.renderTaskPomodoro(pomodoro, $task);
            })
        ;
    },
    storeTask: function($form) {
        var that = this;

        var $nameInput = $form.find("input[name=task-name]"),
            task       = new Task();
        task.setName($nameInput.val().trim());

        that.store("task", task)
            .done(function(task) {
                $nameInput.val("");
                that.renderTask(task);
            })
        ;
    },
    update: function(modelName, model, id) {
        var that = this;

        return $.ajax({
            type: "PUT",
            url: that.baseUrl + modelName + "/" + id,
            data: JSON.stringify(model),
            error: function(jqXHR, textStatus, errorThrown) {
                that.logErrors(jqXHR, textStatus, errorThrown);
            }
        });
    },
    updateLabel: function($form) {
        var that = this;

        var $nameInput   = $form.find("input[name=label-name]"),
            $colourInput = $form.find("input[name=label-colour]"),
            label        = new Label();
        label.setName($nameInput.val().trim());
        label.setColour($colourInput.val().trim().slice(1));
        var id = Util.Store.session("label-id");

        that.update("label", label, id)
            .done(function() {
                $("#page-label a").first().click();
            })
        ;
    },
    updatePomodoro: function($form) {
        var that = this;

        var allIds = _.pluck(Util.Store.session("labels"), "id"); // Zie: http://lodash.com/docs#pluck
        var selectedIds = $form.find("select option[value]:selected").map(function() {
            return Number(this.value);
        }).get();

        var pomodoro = new Pomodoro();
        pomodoro.setDescription($form.find("#pomodoro-description").val());
        pomodoro.addLabels(selectedIds);
        pomodoro.setTimeRemaining(App.Timer.toString(true));

        var id = Util.Store.session("pomodoro-id");
        that.update("pomodoro", pomodoro, id)
            .done(function(pomodoro) {
                console.info(pomodoro);
            })
        ;
    },
    updateTask: function() {
        // @todo updateTask.
    },
    logErrors: function(jqXHR, textStatus, errorThrown) {
        console.group("Error");
            console.error(textStatus);
            console.info(jqXHR);
            console.log(errorThrown);
        console.groupEnd();
    },
    Timer: {
        data: {
            intervalId: null,
            time: null
        },
        render: function() {
            $("#pomodoro-timer").html(this.toString());
        },
        set: function(timeString) {
            var timeArray = timeString.split(":");
            timeArray = _.map(timeArray, function(digit, index) {
                var power = timeArray.length - index - 1;
                return Number(digit) * Math.pow(60, power);
            });
            this.data.time = _.reduce(timeArray, function(sum, num) {
                return sum + num;
            });
            $("#pomodoro-timer")
                .removeClass("ui-icon-clock ui-icon-check")
                .addClass("ui-icon-forbidden")
            ;
            this.render();
        },
        start: function() {
            var that = this;

            this.data.intervalId = setInterval(function() {
                if (0 < that.data.time) {
                    that.data.time--;
                    that.render();
                } else {
                    that.stop();
                    $("#pomodoro-timer")
                        .removeClass("ui-icon-clock")
                        .addClass("ui-icon-check")
                    ;
                    $("#sound-pomodoro-finished")[0].play();
                }
            }, 1000);
            $("#page-pomodoro a[href=#page-tasks]").hide();
        },
        stop: function() {
            window.clearInterval(this.data.intervalId);
            this.data.intervalId = null;
            $("#page-pomodoro a[href=#page-tasks]").show("fast");
        },
        toString: function(includeHours) {
            var timeArray = [];
            if (includeHours) {
                timeArray.push(Math.floor(this.data.time / (60 * 60)));
            }
            timeArray.push(Math.floor(this.data.time / 60));
            timeArray.push(this.data.time % 60);

            return _.map(timeArray, function(digit) {
                return (digit < 10) ? "0" + digit : digit;
            }).join(":");
        }
    },
    templates: {
        label: '<li data-label-id="${label.id}"><a href="#page-label" data-transition="pop"<% if (label.colour) { %> style="background-color: #${label.colour}"<% } %>>${label.name}</a><% if (label.user_id) { %><a data-label-destroy<% if (label.colour) { %> style="background-color: #${label.colour}"<% } %>></a><% } %></li>',
        pomodoroLabel: '<option value="${label.id}"<% if (selected) { %> selected<% } %>>${label.name}</option>',
        pomodoro: '<li data-pomodoro-id="${pomodoro.id}"><a href="#page-pomodoro" data-transition="flow">${pomodoro.description}</a></li>',
        task:
            '<div data-role="collapsible" data-collapsed="true" data-task-id="${task.id}">' +
                '<h3>${task.name}</h3>' +
                '<button class="ui-btn ui-corner-all ui-btn-inline ui-btn-icon-left ui-icon-delete ui-mini">Verwijder ${task.name}</button>' +
                '<ul data-role="listview" data-inset="true">' +
                '</ul>' +
                '<form>' +
                    '<input type="text" name="pomodoro-name" placeholder="Nieuwe Pomodoro toevoegen&hellip;" value="">' +
                '</form>' +
            '</div>'
    }
};
